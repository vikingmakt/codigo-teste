(function (window) {
  var Chart = window.Chart;
  var Masker = window.VMasker;

  var backgroundColor = [
    '#c6dbef',
    '#9ecae1',
    '#6baed6',
    '#3182bd',
    '#08519c',
  ];

  // OPTIONS
  var G_OPTIONS = {
    host: 'http://www.improving.com.br/api/test',
    cep: 'https://viacep.com.br/ws/CEP/json/'
  };

  var GENERAL_ERROR_MESSAGE = "We've encountered a problem. Please try again later";

  // KernelHttp
  var KernelHttp = {};
  KernelHttp.request = function (method, url, data, callback) {
    var request = new XMLHttpRequest();
    request.open(method, url, true);
    request.setRequestHeader('Content-Type', 'application/json');

    request.onreadystatechange = function () {
      if (this.readyState === XMLHttpRequest.DONE) {
        callback({
          status: this.status,
          content: this.responseText
        });
      }
    };
    if (data) {
      request.send(JSON.stringify(data));
    } else {
      request.send();
    }
    request = null;
  };

  // CepApi
  var CepApi = {};
  CepApi.request = function (cep, callback) {
    var url = G_OPTIONS['cep'].replace('CEP', cep);
    KernelHttp.request('GET', url, null, function (response) {
      if (response.status === 200) {
        callback(JSON.parse(response.content));
      }
    });
  };

  // HttpApi
  var HttpApi = {};
  HttpApi.request = function (url, data, callback) {
    url = G_OPTIONS['host'] + url;
    KernelHttp.request('POST', url, data, function (response) {
      response.error = true;

      if (response.status === 200) {
        response.error = false;
        response.content = JSON.parse(response.content);
      }

      return callback(response);
    });
  };

  HttpApi.signup = function (data, callback) {
    HttpApi.request('/users', data, callback);
  };

  HttpApi.cityTemperatures = function (data, callback) {
    HttpApi.request('/city-temperatures', data, callback);
  };

  HttpApi.hitsByBrowser = function (data, callback) {
    HttpApi.request('/hits-by-browser', data, callback);
  };


  // Signup
  var Signup = {};
  Signup.SUCCESS = 200;
  Signup.DATA_ERROR = 400;
  Signup.EMAIL_ERROR = 409;

  Signup.$section = document.getElementById('signup-form');
  Signup.$error = Signup.$section.querySelector('#form-error');

  Signup.formFields = function (form) {
    return ({
      email: form.email.value,
      password: form.password.value,
      fullName: form.fullName.value,
      birthDate: form.birthDate.value,
      zipCode: form.zipCode.value,
      streetName: form.streetName.value,
      number: form.number.value,
      complement: form.complement.value,
      neighbourbood: form.neighbourbood.value,
      country: form.country.value,
      state: form.state.value,
      city: form.city.valuel
    });
  };

  Signup.isValid = function (form) {
    return form.password.value === form.passwordConfirmation.value;
  };

  Signup.maskInput = function () {
    Masker(Signup.$section.querySelector('#birthDate')).maskPattern('99/99/9999');
    Masker(Signup.$section.querySelector('#zipCode')).maskPattern('99999-999');

    Signup.$section.querySelector('#zipCode').addEventListener('blur', function (event) {
      var zipCode = event.target.value.replace('-', '');

      if (zipCode.length !== 8) {
        return;
      }

      CepApi.request(zipCode, Signup.updateAddress);
    });
  };

  Signup.updateAddress = function (data) {
    Signup.$section.querySelector('#city').value = data.localidade;
    Signup.$section.querySelector('#country').value = 'BR';
    Signup.$section.querySelector('#neighbourbood').value = data.bairro;
    Signup.$section.querySelector('#streetName').value = data.logradouro;
  };

  Signup.onSubmit = function (event) {
    event.preventDefault();
    event.stopPropagation();

    if (!Signup.isValid(event.target)) {
      return Signup.showError('Passwords do not match!');
    }

    return HttpApi.signup(Signup.formFields(event.target), function (response) {
      response.form = event.target;
      if (response.status === Signup.SUCCESS) {
        return Signup.success(response);
      }

      if (response.status === Signup.DATA_ERROR) {
        return Signup.dataError(response);
      }

      if (response.status === Signup.EMAIL_ERROR) {
        return Signup.emailError(response);
      }

      return Signup.generalError(response);
    });
  };

  Signup.dataError = function (response) {
    Signup.showError(JSON.parse(response.content).message);
  };

  Signup.emailError = function (response) {
    Signup.showError('This email already exists.');
  };

  Signup.generalError = function (response) {
    Signup.showError(GENERAL_ERROR_MESSAGE);
  };

  Signup.success = function (response) {
    Signup.$section.style.display = 'none';
    Panel.init(response.content);
  };

  Signup.showError = function (message) {
    Signup.$error.querySelector('.message').innerHTML = message;
    Signup.$error.style.display = 'block';
  };

  // Panel
  var Panel = {};
  Panel.SUCCESS = 200;
  Panel.DATA_ERROR = 400;

  Panel.$section = document.getElementById('panel');
  Panel.$error = Panel.$section.querySelector('#panel-error');

  Panel.__cache__ = {};

  Panel.createSection = function (name) {
    return {
      $section: Panel.$section.querySelector('#panel-'+ name + 's'),
      $cityTemperatures: Panel.$section.querySelector('#cityTemperatures-' + name),
      $hitsByBrowser: Panel.$section.querySelector('#hitsByBrowser-' + name),

      hide: function () {
        Panel[name].$section.style.display = 'none';
        Panel[name].$cityTemperatures.innerHTML = 'carregando...';
        Panel[name].$hitsByBrowser.innerHTML = 'carregando...';
      },

      show: function (cityTemperatures, hitsByBrowser) {
        Panel[name].$section.style.display = 'block';
        Panel[name].cityTemperatures(cityTemperatures);
        Panel[name].hitsByBrowser(hitsByBrowser);
      }
    };
  };

  Panel.init = function (data) {
    Panel.__cache__.tableMode = false;
    Panel.$section.style.display = 'block';
    Panel.request(data);
    Panel.table.$tableMode.onclick = Panel.toggleTableMode;
  };

  Panel.showError = function (message) {
    Panel.$error.querySelector('.message').innerHTML = message;
    Panel.$error.style.display = 'block';
  };
  Panel.request = function (data) {
    HttpApi.cityTemperatures(data, function (response) {
      if (response.status !== Panel.SUCCESS) {
        return Panel.showError(GENERAL_ERROR_MESSAGE);
      }

      Panel.__cache__['cityTemperatures'] = Panel.parseCityTemperatures(response.content);
      Panel.chart.cityTemperatures(Panel.__cache__['cityTemperatures']);
      return true;
    });

    HttpApi.hitsByBrowser(data, function (response) {
      if (response.status !== Panel.SUCCESS) {
        return Panel.showError(GENERAL_ERROR_MESSAGE);
      }

      Panel.__cache__['hitsByBrowser'] = Panel.parseHitsByBrowser(response.content);
      Panel.chart.hitsByBrowser(Panel.__cache__['hitsByBrowser']);
      return true;
    });
  };

  Panel.showTableMode = function () {
    Panel.chart.hide();
    Panel.table.show(Panel.__cache__['cityTemperatures'], Panel.__cache__['hitsByBrowser']);
  };

  Panel.hideTableMode = function () {
    Panel.table.hide();
    Panel.chart.show(Panel.__cache__['cityTemperatures'], Panel.__cache__['hitsByBrowser']);
  };

  Panel.toggleTableMode = function (event) {
    event.stopPropagation();
    Panel.__cache__.tableMode = !Panel.__cache__.tableMode;
    Panel.__cache__.tableMode ? Panel.showTableMode() : Panel.hideTableMode();
    Panel.table.$tableMode.classList.toggle('active');
  };


  Panel.parseCityTemperatures = function (data) {
    function average(month) {
      var l = month.length;
      var t = month.reduce(function (total, day) {
        return total + day;
      });

      return (t / l);
    }

    function parseMonth(row) {
      row.data = row.data.reduce(function (months, day){
        var m = day[0].split('-')[1];
        var t = day[1];
        months[m] = months[m] || [];
        months[m].push(t);
        return months;
      }, {});
      return row;
    }

    function calcAverage(row) {
      row.data = Object.keys(row.data).map(function (m) {
        return {
          month: m,
          average: average(row.data[m])
        };
      }).sort(function (a, b) {
        return a.month > b.month;
      }).map(function (month) {
        return month.average.toFixed(1);
      });

      return row;
    }

    return data
      .map(parseMonth)
      .map(calcAverage);
  };

  Panel.parseHitsByBrowser = function (data) {
    var total = data.reduce(function (t, browser) {
      return t + browser[1];
    }, 0);

    function percentage(t) {
      return ((t / total) * 100).toFixed(2);
    }

    return data.map(function (row) {
      return {
        name: row[0],
        value: percentage(row[1])
      };
    });
  };

  // Panel.chart.*

  Panel.chart = Panel.createSection('chart');

  Panel.chart.__cache__={};

  Panel.chart.cityTemperatures = function (data) {
    if (Panel.chart.__cache__.cityTemperatures) return;

    var datasets = data.map(function (d, i) {
      return {
        label: d.name,
        data: d.data,
        backgroundColor: backgroundColor[i]
      };
    });

    Panel.chart.__cache__.cityTemperatures = new Chart(Panel.chart.$cityTemperatures, {
      type: 'bar',
      data: {
        labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        datasets: datasets
      }
    });
  };

  Panel.chart.hitsByBrowser = function (data) {
    if (Panel.chart.__cache__.hitsByBrowser) return;

    var labels = data.map(function (d) { return d.name; });
    var dataset = data.map(function (d) { return d.value; });

    Panel.chart.__cache__.hitsByBrowser = new Chart(Panel.chart.$hitsByBrowser, {
      type: 'pie',
      data: {
        labels: labels,
        datasets: [{
          data: dataset,
          backgroundColor: backgroundColor
        }]
      }
    });
  };

  // Panel.table.*

  Panel.table = Panel.createSection('table');
  Panel.table.$tableMode = Panel.$section.querySelector('#tableMode');
  Panel.table.tpl = {
    cityTemperatures: document.getElementById('tpl-cityTemperatures').innerHTML,
    hitsByBrowser: document.getElementById('tpl-hitsByBrowser').innerHTML
  };

  Panel.table.cityTemperatures = function (data) {
    Panel.table.$cityTemperatures.innerHTML = Panel.table.tpl.cityTemperatures;
    Panel.table.$cityTemperatures.querySelector('tbody').innerHTML = Panel.table.cityTemperaturesRows(data);
  };

  Panel.table.cityTemperaturesRows = function (data) {
    return data.map(function (row) {
      var months = row.data.map(function (month) {
        return '<td>' + month.replace('.', ',') + '</td>';
      }).join('');

      return '<tr><td>'+ row.name +'</td>'+ months + '</tr>';
    }).join('');
  };

  Panel.table.hitsByBrowser = function (data) {
    Panel.table.$hitsByBrowser.innerHTML = Panel.table.tpl.hitsByBrowser;
    Panel.table.$hitsByBrowser.querySelector('tbody').innerHTML = Panel.table.hitsByBrowserRows(data);
  };

  Panel.table.hitsByBrowserRows = function (data) {
    return data.map(function (row) {
      return '<tr><td>'+ row.name + '</td><td>'+ row.value.replace('.', ',') +'</td></tr>';
    }).join('');
  };

  // bootstrap
  document.addEventListener('DOMContentLoaded', function () {
    Signup.maskInput();
    document.forms[0].onsubmit = Signup.onSubmit;
  });
})(window);
